import $ from 'jquery';

const checkbox = document.querySelector('#company_details');
const icFieldInput = document.getElementById('billing_ic');

const updateIcDicFields = () => {
	const autofill = document.querySelector('#wpify-woo-ares-autofill');
	const dicDph = document.querySelector('#billing_dic_dph_field');
	const country = document.querySelector('#billing_country');

	if (autofill) {
		if (country && country.value === 'CZ') {
			autofill.style.display = 'block';
		} else {
			autofill.style.display = 'none';
		}
	}

	if (country && country.value !== 'SK') {
		dicDph.style.display = 'none';
	} else if (country && country.value === 'SK') {
		if (checkbox && checkbox.checked === false) {
			dicDph.style.display = 'none';
		} else {
			dicDph.style.display = 'block';
		}
	}

	updateRequiredFields();
};


const showFields = () => {
	const fields = Array.from(document.getElementsByClassName('wpify-woo-ic-dic__company_field'));
	const country = document.querySelector('#billing_country');

	fields.forEach((item) => {
		if (item.id === 'billing_dic_dph_field' && country.value !== 'SK') {
			item.style.display = 'none';
		} else {
			item.style.display = 'block';
		}
	});
};

const setAsRequired = (field) => {
	const span = field.querySelector('label span');
	field.classList.add("validate-required");
	span.className = "required";
	span.innerText = "*";
}

const setAsOptional = (field) => {
	const span = field.querySelector('label span');
	field.classList.remove("validate-required");
	span.className = "optional";
	span.innerText = wpifyWooIcDic.optionalText;
}

const updateRequiredFields = () => {
	const companyFieldInput = document.getElementById('billing_company');
	const companyField = document.getElementById('billing_company_field');
	const icField = document.getElementById('billing_ic_field');

	if ( wpifyWooIcDic.requireCompany ) {
		if (checkbox && checkbox.checked ) {
			setAsRequired(companyField)
		} else {
			setAsOptional(companyField)
		}
	}

	if ( wpifyWooIcDic.requireVatFields !== null ) {
		if ((checkbox && checkbox.checked && wpifyWooIcDic.requireVatFields === 'if_checkbox')
			|| (companyFieldInput.value !== '' && wpifyWooIcDic.requireVatFields === 'if_company')) {
			setAsRequired(icField)
		} else {
			setAsOptional(icField)
		}
	}
}

if (checkbox) {
	let fields = Array.from(document.getElementsByClassName('wpify-woo-ic-dic__company_field'));
	let inputs = Array.from($('.wpify-woo-ic-dic__company_field input'));

	window.addEventListener('load', (event) => {
		if (icFieldInput.value) {
			checkbox.checked = true;
			showFields();
			updateRequiredFields();
		}

		if (checkbox.checked === false) {
			fields.forEach((item) => item.style.display = 'none');
			inputs.forEach((item) => item.value = '');
		}
	});

	checkbox.addEventListener('change', (event) => {
		if (event.target.checked) {
			showFields();
			updateRequiredFields();
		} else {
			fields.forEach((item) => item.style.display = 'none');
			inputs.forEach((item) => item.value = '');
			updateRequiredFields();
		}
	});
}

if (wpifyWooIcDic.requireVatFields !== null && wpifyWooIcDic.requireVatFields === 'if_company') {
	const companyFieldInput = document.getElementById('billing_company');

	companyFieldInput.addEventListener('change', function () {
		updateRequiredFields();
	});
}

const aresShowBtn = document.getElementById('wpify-woo-icdic__ares-autofill-button');

if (aresShowBtn) {
	aresShowBtn.addEventListener('click', function (e) {
		e.preventDefault();
		document.getElementsByClassName('wpify-woo-icdic__ares-autofill')[0].style.display = 'block';
	});
}

const aresBtn = document.getElementById('wpify-woo-icdic__ares-submit');

if (aresBtn) {
	document.getElementById('ares_in').value = icFieldInput.value;
	icFieldInput.addEventListener('keyup', function () {
		document.getElementById('ares_in').value = icFieldInput.value;
	});
	aresBtn.addEventListener('click', function () {
		const ino = document.getElementById('ares_in').value;
		return fetch(`${wpifyWooIcDic.restUrl}/icdic?in=${ino}`)
			.then(res => {
				if (!res.ok) {
					return res.json().then(json => {
						document.getElementById('wpify-woo-icdic__ares-result').innerHTML = json.message;
					});
				}

				res.json().then(json => {
					const details = json.details;

					Object.keys(details).forEach(function (key) {
						const element = document.getElementById(key);

						if (element) {
							element.value = details[key];
							element.dispatchEvent(new Event('change'));
						}
					});

					const checkbox = document.getElementById('company_details');

					if (checkbox) {
						checkbox.checked = true;
					}

					document.getElementById('wpify-woo-icdic__ares-result').innerHTML = '';
					if (wpifyWooIcDic.position !== 'after_ic_field') {
						document.getElementsByClassName('wpify-woo-icdic__ares-autofill')[0].style.display = 'none';
					}

					showFields();
				});
			})
			.catch(console.error);
	});
}

const removeIcErrorMessage = () => {
	Array.from(document.querySelectorAll('.wpify-woo__ic-error')).forEach(function (item) {
		item.parentNode.removeChild(item);
	});
};

if (document.getElementsByClassName('wpify-woo-ic--validate').length) {
	const countryField = document.getElementById('billing_country');

	icFieldInput.addEventListener('change', function () {
		if (countryField.value !== 'CZ') {
			removeIcErrorMessage();
			return;
		}

		if (!icFieldInput.value) {
			removeIcErrorMessage();
			return false;
		}

		return fetch(`${wpifyWooIcDic.restUrl}/icdic?in=${icFieldInput.value}`)
			.then(res => {
				if (!res.ok) {
					return res.json().then(json => {
						removeIcErrorMessage();
						const el = document.createElement('div');
						el.innerHTML = json.message;
						el.classList.add('wpify-woo__ic-error');
						icFieldInput.parentNode.insertBefore(el, icFieldInput.nextSibling);
						document.getElementById('wpify-woo-icdic__ares-result').innerHTML = json.message;
					});
				}

				res.json().then(json => {
					removeIcErrorMessage();
					document.getElementById('billing_dic').value = json.details.billing_dic;
					document.getElementById('billing_company').value = json.details.billing_company;
				});
			})
			.catch(console.error);
	});
}

const removeViesErrorMessage = () => {
	Array.from(document.querySelectorAll('.wpify-woo__vies-error')).forEach(function (item) {
		item.parentNode.removeChild(item);
	});
};

if (document.getElementsByClassName('wpify-woo-vies--validate').length) {
	const country = document.querySelector('#billing_country');
	let dicFieldInput = document.getElementById('billing_dic');

	if (country && country.value === 'SK') {
		dicFieldInput = document.getElementById('billing_dic_dph');
	}

	dicFieldInput.addEventListener('change', function () {

		if (!dicFieldInput.value) {
			removeViesErrorMessage();
			return false;
		}

		return fetch(`${wpifyWooIcDic.restUrl}/icdic-vies?in=${dicFieldInput.value}`)
			.then(res => {
				if (!res.ok) {
					return res.json().then(json => {
						removeViesErrorMessage();
						const el = document.createElement('div');
						el.innerHTML = json.message;
						el.classList.add('wpify-woo__vies-error');
						dicFieldInput.parentNode.insertBefore(el, dicFieldInput.nextSibling);
					});
				}

				res.json().then(json => {
					removeViesErrorMessage();
				});
			})
			.catch(console.error);
	});
}

$('body').on('change', '#billing_dic,#billing_dic_dph,#company_details', function() {
	$('body').trigger('update_checkout');
});

$(document.body).on('updated_checkout', updateIcDicFields);
