<?php

namespace WpifyWoo\Modules\QRPayment;

use DateTime;
use Exception;
use WC_Email;
use WC_Order;
use WpifyWoo\Abstracts\AbstractModule;
use WpifyWooDeps\Rikudou\CzQrPayment\Options\QrPaymentOptions;
use WpifyWooDeps\Rikudou\CzQrPayment\QrPayment;
use WpifyWooDeps\Rikudou\Iban\Iban\CzechIbanAdapter;
use WpifyWooDeps\Rikudou\Iban\Iban\IBAN;

class QRPaymentModule extends AbstractModule
{
	/**
	 * @return void
	 */
	public function setup()
	{
		add_filter('wpify_woo_settings_' . $this->id(), array($this, 'settings'));
		add_action('template_redirect', [ $this, 'display_qr_code_on_thankyou' ] );
		add_action('woocommerce_email_before_order_table', [$this, 'display_qr_code_in_email'], 20, 4 );
		add_action('wpify_woo_render_qr_code', [$this, 'render_qr_code']);
	}

	public function display_qr_code_on_thankyou() {
		if ( is_checkout() && !empty( is_wc_endpoint_url('order-received') ) ) {
			$position = $this->get_setting( 'thankyou_position' );

			if ( !empty($position) && $position === 'dont_show') {
				return;
			}

			if ( $position === 'before_order' ) {
				add_action('woocommerce_thankyou', [$this, 'render_qr_code'], 1, 1);
			} elseif ( $position === 'after_thankyou' ) {
				add_action('woocommerce_thankyou', [$this, 'render_qr_code'], 20, 1);
			} else {
				add_action('woocommerce_before_thankyou', [$this, 'render_qr_code'], 10, 1);
			}
		}
	}

	/**
	 * Module ID
	 * @return string
	 */
	public function id(): string
	{
		return 'qr_payment';
	}

	/**
	 * Module settings
	 * @return array[]
	 */
	public function settings(): array
	{
		$currencies = [];
		foreach (get_woocommerce_currencies() as $key => $val) {
			$currencies[] = [
				'label' => $val,
				'value' => $key,
			];
		}

		$emails = [];
		foreach (WC()->mailer()->get_emails() as $wc_email) {
			$emails[] = [
				'label' => $wc_email->title . ' - ' . esc_html($wc_email->is_customer_email() ? __('Customer', 'woocommerce') : $wc_email->get_recipient()),
				'value' => $wc_email->id,
			];
		}

		$countries = [];
		foreach (WC()->countries->get_allowed_countries() as $key => $val) {
			$countries[] = [
				'label' => $val,
				'value' => $key,
			];
		}

		$settings = array(
			array(
				'id' => 'payment_methods',
				'type' => 'multi_group',
				'label' => __('Enabled payment methods', 'wpify-woo'),
				'items' => [
					[
						'id' => 'payment_method',
						'type' => 'select',
						'label' => __('Payment method', 'wpify-woo'),
						'options' => $this->plugin->get_woocommerce_integration()->get_gateways(),
					],
					[
						'id' => 'enabled_emails',
						'type' => 'multi_select',
						'label' => __('Show in emails', 'wpify-woo'),
						'options' => $emails,
					],
					[
						'id' => 'accounts',
						'type' => 'multi_group',
						'label' => __('Accounts', 'wpify-woo'),
						'items' => [
							[
								'id' => 'number',
								'type' => 'text',
								'label' => __('Account number', 'wpify-woo'),
							],
							[
								'id' => 'bank_code',
								'type' => 'text',
								'label' => __('Bank Code', 'wpify-woo'),
							],
							[
								'id' => 'iban',
								'type' => 'text',
								'label' => __('IBAN - required for SK payments', 'wpify-woo'),
							],
							[
								'id' => 'enabled_currencies',
								'type' => 'multi_select',
								'label' => __('Enabled currencies', 'wpify-woo'),
								'options' => $currencies,
							],
							[
								'id' => 'type',
								'type' => 'select',
								'label' => __('QR Type', 'wpify-woo'),
								'options' => [
									[
										'label' => 'CZ - QR Platba',
										'value' => 'cz',
									],
									[
										'label' => 'SK - Pay BY Square',
										'value' => 'sk',
									],
								],
							],
							[
								'id' => 'enabled_countries',
								'type' => 'multi_select',
								'label' => __('Enabled countries', 'wpify-woo'),
								'options' => $countries,
							],
						],
					],
				],
			),
			[
				'id' => 'note',
				'label' => __('Message to recipient', 'wpify-woo'),
				'desc' => __('Enter the message to recipient if you want it. You can use codes {order} to insert the order number and {shop_name} to insert the name of the shop. So the message might look like, for example, "QR payment order {order} from {shop_name}".', 'wpifywoo'),
				'type' => 'text',
			],
			[
				'id' => 'title_before',
				'label' => __('Title before', 'wpify-woo'),
				'type' => 'wysiwyg',
			],
			[
				'id' => 'title_after',
				'label' => __('Title after', 'wpify-woo'),
				'type' => 'wysiwyg',
			],
			array(
				'id'            => 'thankyou_position',
				'label'         => __( 'QR position on thank you page', 'wpify-woo' ),
				'desc'          => __( 'Select where the QR code should be placed on the thank you page.', 'wpify-woo' ),
				'type'          => 'select',
				'options' => [
					[
						'label' => __( 'Dont show', 'wpify-woo' ),
						'value' => 'dont_show',
					],
					[
						'label' => __( 'Before page content', 'wpify-woo' ),
						'value' => 'before_thankyou',
					],
					[
						'label' => __( 'Before Order details', 'wpify-woo' ),
						'value' => 'before_order',
					],
					[
						'label' => __( 'After page content', 'wpify-woo' ),
						'value' => 'after_thankyou',
					],
				],
			),
			[
				'id' => 'compatibility_mode',
				'label' => __('Compatibility mode', 'wpify-woo'),
				'desc' => __('The SK version requires XZ utils (https://tukaani.org/xz/). If your serevr does not support this, you can enable the compatibility mode, in which the QR code will be generated using the external API QR-Platba.cz and QRGenerator.sk. This is not recommended for performance reasons, as an unnecessary API call is done on thankyou page.',
					'wpifywoo'),
				'type' => 'toggle',
			],

		);

		return $settings;
	}

	/**
	 * Module name
	 * @return string
	 */
	public function name(): string
	{
		return __('QR Payment', 'wpify-woo');
	}

	/**
	 * Render QR code
	 *
	 * @param int|WC_Order $order
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function render_qr_code($order)
	{
		if (is_numeric($order)) {
			$order = wc_get_order($order);
		}

		$payment_method = $order->get_payment_method();
		$currency = $order->get_currency();
		$country = $order->get_billing_country();
		$accounts = [];

		foreach ($this->get_setting('payment_methods') as $item) {
			if (empty($item['payment_method']) || $payment_method !== $item['payment_method']) {
				continue;
			}

			foreach ($item['accounts'] as $account) {
				if (!empty($account['enabled_currencies']) && !in_array($currency, $account['enabled_currencies'])) {
					continue 2;
				}
				$enabled_countries = !empty($account['enabled_countries']) ? $account['enabled_countries'] : array_keys(WC()->countries->get_allowed_countries());
				if (!in_array($country, $enabled_countries)) {
					continue;
				}
				$accounts[] = $account;
			}
		}
		if (empty($accounts)) {
			return;
		}

		$qrCodes = array();
		$note = $this->get_setting('note');
		$note_text = '';

		if ($note) {
			$replaces = [
				'{order}' => $order->get_order_number(),
				'{shop_name}' => get_bloginfo('name'),
			];
			$note_text = str_replace(array_keys($replaces), array_values($replaces), $note);
		}

		foreach ($accounts as $key => $account) {


			$payment_details = [
				'total' => $order->get_total(),
				'vs' => $order->get_order_number(),
				'currency' => $order->get_currency(),
				'due_date' => date('Y-m-d'),
				'account_number' => $account['number'] ?? '',
				'bank_code' => $account['bank_code'] ?? '',
				'iban' => $account['iban'] ?? '',
				'note' => $note_text,
			];
			$payment_details = apply_filters('wpify_woo_qr_payment_details', $payment_details);

			if (!$this->get_setting('compatibility_mode')) {
				if ('cz' === $account['type']) {
					$payment = new QrPayment(new CzechIbanAdapter($payment_details['account_number'], $payment_details['bank_code']), [
						QrPaymentOptions::VARIABLE_SYMBOL => $payment_details['vs'],
						QrPaymentOptions::AMOUNT => $payment_details['total'],
						QrPaymentOptions::CURRENCY => $payment_details['currency'],
						QrPaymentOptions::DUE_DATE => new DateTime($payment_details['due_date']),
						QrPaymentOptions::COMMENT => $payment_details['note'],
					]);
					$qrCodes[$key] = $payment->getQrCode()->getDataUri();
				} elseif ('sk' === $account['type']) {
					$payment = new \WpifyWooDeps\rikudou\SkQrPayment\QrPayment();
					$payment->setOptions([
						QrPaymentOptions::AMOUNT => $payment_details['total'],
						QrPaymentOptions::CURRENCY => $payment_details['currency'],
						QrPaymentOptions::DUE_DATE => new DateTime($payment_details['due_date']),
						QrPaymentOptions::VARIABLE_SYMBOL => $payment_details['vs'],
						QrPaymentOptions::COMMENT => $payment_details['note'],
						\WpifyWooDeps\rikudou\SkQrPayment\Payment\QrPaymentOptions::IBANS => [
							new IBAN($payment_details['iban']),
						],
					]);
					$qrCodes[$key] = $payment->getQrCode()->getDataUri();
				}
			} else {
				if ('cz' === $account['type']) {
					$account_prefix = '';
					$account_number = $payment_details['account_number'];
					if (str_contains($account_number, '-')) {
						$ex = explode('-', $account_number);
						$account_prefix = $ex[0];
						$account_number = $ex[1];
					}
					$url = add_query_arg([
						'accountPrefix' => $account_prefix,
						'accountNumber' => $account_number,
						'bankCode' => $payment_details['bank_code'],
						'amount' => $payment_details['total'],
						'currency' => $payment_details['currency'],
						'vs' => $payment_details['vs'],
						'message' => $payment_details['note'],
					], 'https://api.paylibo.com/paylibo/generator/czech/image');
					$qrCodes[$key] = base64_encode(wp_remote_retrieve_body(wp_remote_get($url)));
					$qrCodes[$key] = "data:image/png;base64,{$qrCodes[$key]}";
				} elseif ('sk' === $account['type']) {
					$url = add_query_arg([
						'iban' => $payment_details['iban'],
						'bankCode' => $payment_details['bank_code'],
						'amount' => $payment_details['total'],
						'currency' => $payment_details['currency'],
						'vs' => $payment_details['vs'],
						'payment_note' => $payment_details['note'],
						'format' => 'png',
						'size' => 256,
					], 'https://api.qrgenerator.sk/by-square/pay/base64');
					$qrCodes[$key] = json_decode(wp_remote_retrieve_body(wp_remote_get($url)));
					$qrCodes[$key] = "data:{$qrCodes[$key]->mime};base64,{$qrCodes[$key]->data}";
				}
			}
		}

		if ($qrCodes) {
			echo '<div class="wpify-woo-qr-payment">';
			echo sprintf('<div class="wpify-woo-qr-payment_title-before">%s</div>', $this->get_setting('title_before'));
			foreach ($qrCodes as $qrCode) {
				echo "<img  class='wpify-woo-qr-payment_code' src='{$qrCode}' style='display: inline-block'>";
			}
			echo sprintf('<div class="wpify-woo-qr-payment_title-after">%s</div>', $this->get_setting('title_after'));
			echo '</div>';
		}
	}

	/**
	 * Display QR code in emails
	 *
	 * @param int|WC_Order $order
	 * @param WC_Email $email
	 *
	 * @throws Exception
	 */
	public function display_qr_code_in_email($order, $sent_to_admin, $plain_text, $email)
	{
		foreach ($this->get_setting('payment_methods') as $item) {
			if ( empty($item['enabled_emails']) || !in_array( $email->id, $item['enabled_emails'] ) ) {
				return;
			}
			$this->render_qr_code($order);
		}
	}
}
