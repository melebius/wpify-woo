module.exports = {
	wordPressUrl: 'https://www.wpify-woo.test',
	config: {
		build: 'build',
		entry: {
			'settings': [
				'./assets/admin/settings.js',
				'./assets/admin/settings.scss',
			],
			'icdic': [
				'./src/Modules/IcDic/assets/icdic.js',
				'./src/Modules/IcDic/assets/icdic.scss',
			],
			'packeta': [
				'./src/Modules/PacketaShipping/assets/packeta.js',
				'./src/Modules/PacketaShipping/assets/packeta.scss',
			],
			'packeta-admin': [
				'./src/Modules/PacketaShipping/assets/packeta-admin.js',
				'./src/Modules/PacketaShipping/assets/packeta-admin.scss',
			]
		},
	},
};
